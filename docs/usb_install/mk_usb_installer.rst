.. _mk_usb_installer:

``mk_usb_installer.sh`` documentation
-------------------------------------

.. NOTE::
   This script uses ``sudo`` to write to the USB key, so you may be prompted
   for a ``sudo`` password.

Usage
^^^^^

The mk_usb_installer.sh script has two parameters:

``usb-device``: path to the usb device (for example: ``/dev/sdb1``).

``config-filename``: (optional) configuration file containing the settings to build the machine and run ansible.

You can call it this way::

    $ ./mk_usb_installer.sh usb-device config-filename

Configuration File
^^^^^^^^^^^^^^^^^^

``hostname``: (base). Hostname of the target machine. (key to ansible)

``url``: (video-setup.debian.net). The location of the http server hosting the preseed configuration and additional files. See `hosting_files`_ below.

``distro``: (debian) debian or ubuntu

``suite``: (bookworm). Distribution suite to use.

``arch``: (amd64). Architecture of the target machine.

``archive_mirror``: (http://deb.debian.org) Where to get installer image.

``cdimage_mirror``: (https://cdimage.debian.org) URL to the directory containing the ISO file.

``load_firmware``: (false). Boolean. When true, d-i will prompt the user to provide non-free firmware blobs.

``more_appends``: (blank). Additional configuration to be appended to the kernel command line.

Kernel parameters
^^^^^^^^^^^^^^^^^

``lc2_url``: If passed, late_command.sh will wget and run it instead of late_command.sh.

About Ubuntu
^^^^^^^^^^^^^

Ubuntu support is here for anyone that wants it, but don't expect it to always
just work. The URL patterns, hd-media fs type, free space and ISO size don't
always fit, the d-i preseed options are slightly different, etc. It is a
handy platform for testing things in Launchpad PPAs, or simply testing on
Ubuntu.

Ubuntu needs ver set in addition to suite, including the point release. (Debian does not need the ver/point release specified.)

``ver``: (no default). Current numeric Version of suite. Example: ``18.04.3``

.. _hosting_files:

Hosting Files
^^^^^^^^^^^^^

The installer needs to get configuration files from somewhere. There are many
simple solutions, but they all have their downsides and your lan setup may make
some easy or impossible. This section describes a few of the solutions. For more
details see the Debian `preseed page`_.

You will know you are having problems when the installer says:
 ``Failed to retrieve the preconfigureation file``

There are 2 parts: where to host the files, and how the installer will reference them.

The default is is to use ``video-setup.debian.net`` which redirects to the
video-team git repo on salsa. This makes :ref:`Quick Start <usb\_quick\_start>`
easy, but isn't practical for production.

For R&D or production, you can use a box on your lan by cloning the repo and
``http_server.sh`` after ``mk_usb_installer.sh`` and set ``url=($hostname:8007)``
to the installer. This will work if the installer can resolve hostname.domain
and wget the files from it.

If it can't resolve, it may be the domain. Try ``.local`` Or use the IP address:
``url=1.2.3.4:8007``

You can also host the files on some other server, using some other protocol, or
put the files on the usb stick (this will requires editing some scripts to
replace wget with cp.)

You'll want to customize
``roles/pxe/tftp-server/files/scripts/late_command.cfg`` to point to
your correct repos and branches.

.. _`preseed page`: https://www.debian.org/releases/stable/amd64/apbs02.en.html#preseed-loading
