.. _usb_or_pxe:

USB vs PXE installer
====================

Although :ref:`our advanced usage documentation <advanced\_usage>` covers
imaging via PXE, you will get your first box up and running quicker with less
fuss using the USB installer. It's also useful to bootstrap the PXE server.

We try to make the processes similar, and use as many of the same files as is
practical.

In both cases the installer will install a preseeded image, reboot, run ansible
and reboot again.

Hosting Files
-------------

Both USB and PXE booted installers need to get configuration files from
somewhere.

The PXE solution is very opinionated. The PXE server controls all the things,
hosts the files, we can assume it will work just fine.

The USB solution needs to make use of the exiting network.

There are many simple solutions, but they all have their downsides and your lan
setup may make some easy or impossible. This section describes a few of the
solutions. For more details see the Debian `preseed page`_.

You will know you are having problems when the installer says: ``Failed to
retrieve the preconfiguration file``

There are 2 parts: where to host the files, and how the installer will
reference them.

Passing a hostname of a web server that hosts the files is the typical solution.

The default for USB is 'url=video-setup.debian.net` which redirects to `files
in our tftp-server ansible role`_. This is a bit magical but is the most
reliable way to host files needed for the Quick Start demo setup.  It works out
of the box.

You can run ``mk_usb_installer.sh`` and pass $hostname:8007 to the installer.
This will work if the installer can resolve hostname.domain and wget the files
from your local machine.  When this works it is fine, but may not work out of
the box as networks differ.

If it can't resolve, it may be the domain. Try ``.local`` or use the IP
address: ``1.2.3.4:8007``.

You can also host the files on some other server, using some other protocol, or
put the files on the usb stick (this will requires editing some scripts to
replace wget with cp.)

.. _`preseed page`: https://www.debian.org/releases/stable/amd64/apbs02.en.html#preseed-loading
.. _`files in our tftp-server ansible role`: https://salsa.debian.org/debconf-video-team/ansible/-/tree/master/roles/pxe/tftp-server/files

FAQ like things
---------------

PXE and USB are both boot devices which boot the same kernel, installer, which
use the same preseed files and ansible playbooks to build up a box.

The goal is for both to share as much as possible, but there are some
differences resulting in some duplication of settings and different behavior.

Both require a basic connection to the Internet.

Both take about the same amount of time (most of the time is spent downloading
and decompressing .debs and writing to disk. Installing to an SSD is the best
way to speed this up.

Differences:

PXE relies on a DHCP server sending parameters that reference a TFTP server.

PXE assumes the same server will host DHCP, TFTP and HTTP (sort of.)

PXE assumes you either know what disk is going to be wiped, or you don't care
and will try again if you get it wrong.

PXE can be 100% managed with ansible inventory files. (once you get it up and
running, which is best done with a USB install.)

PXE requires collecting mac addresses and assigning hostnames in the ansible
inventory files.

USB will prompt for some networking and disk parameters, and the HTTP server is
configurable.

USB installer needs to get files from a local http server. Setting up the
server is easy, resolving it from the installer doesn't always "just work".

Both support UEFI, but some systems won't do UEFI PXE, in that case you have to
use USB if you want an UEFI install.

USB is easier to get started with, PXE is nicer to work with.
