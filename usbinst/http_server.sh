#!/bin/bash -ex

# serves d-i/foo/preseed, scripts/{early,late}_command.sh, etc.

# New in version 3.7: --directory specify alternate directory
# so don't use this, yet.
# python3 -m http.server 8007 --directory ../roles/pxe/tftp-server/files/
cd ../roles/pxe/tftp-server/files/
python3 -m http.server 8007
