#!/bin/bash

# set -ex

# setup:
### sudo apt install qemu qemu-utils qemu-system ovmf

# give user rw to usb device
### sudo adduser $USER disk
# (need to logout/in for this to take affect.)
# or do some other trick that I don't think will let the script run :p

# operation:
# ./test_thumb.sh /dev/sdX
# $1 = dev of usb stick
thumb=$1

# This will boot the USB stick, install to disk.cow.
# Note the hard drive will be /dev/vda

# It does this twice, once for UEFI and again for legacy (bios).

# for UEFI, it should:
# 1. find the bootable thumb drive,
# 2. install OS and first_boot.sh, reboot
# 3. find the bootable disk.cow, boot the OS, run firs_boot.sh, reboot
# 4. find the bootable disk.cow, boot the OS, do whatever ansible set it up to do.

# For legacy, you need to help it figure out what disks to boot.
# You will see "Press ESC for boot menu."
# press ESC, select
# 2. USB MSC Drive QEMU QEMU HARDDISK 2.5+
# when the installer is done and it reboots,
# press ESC, select
# 1. Virtio disk PCI:00:03.0
# (these boot steps will go away if someone can figure out -boot once)

# For both UEFI and legacy (and bare metal),
# the installer will ask which drive to install the os and grub to,
# pick vda:

# Partitioning method:
#       Guided - use entire disk

# Select disk to partition:
#       SCSI7 (0,0,0) (sda) - 15.8 GB QEMU QEMU HARDDISK
#       Virtual disk 1 (vda) - 10.7 GB Virtio Block Device

# Device for boot loader installation:
#       Enter device manually
#       /dev/sda  (usb-QEMU_QEMU_HARDDISK_1-0000:00:1d.7-1-0:0)
#       /dev/vda


# blank disk to install to
qemu-img create -f qcow2 disk.cow 10G

# uefi
qemu-system-x86_64 -m 512 \
    -machine q35,accel=kvm \
    -cpu host \
    -bios OVMF.fd \
    -drive file=disk.cow,if=virtio,cache=unsafe \
    -drive file=${thumb},if=none,format=raw,id=thumb \
    -usb \
    -device usb-storage,drive=thumb


qemu-img create -f qcow2 disk.cow 10G

# legacy
qemu-system-x86_64 -m 512 \
    -machine q35,accel=kvm \
    -cpu host \
    -drive file=disk.cow,if=virtio,cache=unsafe \
    -drive file=${thumb},if=none,format=raw,id=thumb \
    -usb \
    -device usb-storage,drive=thumb \
    -boot menu=on


