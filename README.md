# Setup and Testing the Setup

There are a few setup workflows that try to share common files.
There are different preseed and `late_comand.sh` files for Debian/Ubuntu and
USB/PXE, they are all very similar, maybe they can be merged later. The
playbooks defined in this repo require ansible version 2.4 or greater to run.
In Debian Stretch, this is available in backports.

Detailed instruction on how to use this repo are
[available](https://debconf-video-team.pages.debian.net/ansible)

1. Clone repo to your local machine.
2. Edit `inventory/hosts` and `inventory/{group,host}_vars`
3. Decide where the files will be hosted. The choices are:
     - Local machine using http server
     - Some other machine using http, either local or the cloud
     - Copy to USB stick (only makes sense for USB stick install)
     - Copy to PXE server (only make sense for PXE installs)

4. Make a USB stick. Even if you are going to do PXE installs, you need to build
   the PXE server using a USB. Details on how to create the USB stick can be
   found [here](https://debconf-video-team.pages.debian.net/ansible/usb_install/mk_usb_installer.html)
5. Boot a target machine from USB stick. The installer will ask for a hostname.
   That will install OS and run Ansible for that `$hostname`.

The ansible command to use is:

    ansible-playbook -i inventory/hosts site.yml -u root

The result is a machine ready to be used in production.

# Using vagrant for development

There is a Vagrantfile in this directory, which will allow you to create
a small development environment in virtual machines. To get started,
just run

    vagrant up

which will create a number of VMs in your chosen hypervisor and run
ansible against them.

If you made changes to any of the roles and want to rerun ansible
without having to start from scratch, just run `vagrant up` a second
time. Alternatively, if you do want to start from scratch, then run

    vagrant destroy -f

which will delete all the VMs. You can then recreate them with another
`vagrant up` command.

If you want to add more machines or change the roles assigned to one or
more of them, edit `Vagrantfile` (a ruby source file):

* The `staticips` hash contains an entry for each machine that will be
  added. Note that this is in the format that the `staticips` role
  expects.
* The `ansible.groups` configuration item says which hosts to assign to
  which group. The format is `"groupname" =>
  ["hostname1","hostname2",...]`.
* The `ansible.host_vars` configuration item allows you to override
  variables for certain hosts.
* The `store.transaction` generates a YAML file with global variables.

For more information, see the [vagrant
documentation](https://www.vagrantup.com/docs/provisioning/ansible_intro)
