---
- name: Create nginx content directories
  ansible.builtin.file:
    path: "{{ item }}"
    state: directory
    owner: www-data
    group: www-data
    mode: "0755"
  with_items:
    - "{{ streaming.backend.data_root }}"
    - "{{ streaming.backend.data_root }}/www"
    - "{{ streaming.backend.data_root }}/hls"
    - "{{ streaming.backend.data_root }}/dump"
  tags:
    - streaming

- name: Use tmpfs for hls
  ansible.builtin.lineinfile:
    dest: /etc/fstab
    line: "tmpfs {{ streaming.backend.data_root }}/hls tmpfs \
           uid=www-data,gid=www-data,mode=0755 0 2"
    regexp: \s{{ streaming.backend.data_root }}/hls\s
  notify: Mount all
  tags:
    - streaming

- name: Create nginx dump directories for rooms
  ansible.builtin.file:
    path: "{{ streaming.backend.data_root }}/dump/{{ item }}"
    state: directory
    owner: www-data
    group: www-data
    mode: "0755"
  with_items: "{{ streaming.rooms }}"
  when: streaming.dump
  tags:
    - streaming

- name: Create nginx dash directories for rooms
  ansible.builtin.file:
    path: "{{ streaming.backend.data_root }}/dash/{{ item }}"
    state: directory
    owner: www-data
    group: www-data
    mode: "0755"
  with_items: "{{ streaming.rooms }}"
  when: streaming.dash is defined
  tags:
    - streaming

- name: Install nginx streaming backend http configuration
  ansible.builtin.template:
    src: nginx/sites-enabled/streaming-backend-http.conf.j2
    dest: /etc/nginx/sites-enabled/streaming-backend-http.conf
    owner: root
    group: root
    mode: "0644"
  notify: Reload nginx
  tags:
    - streaming

- name: Force nginx reload before tls cert handling
  ansible.builtin.meta: flush_handlers

- name: Generate tls certificate
  ansible.builtin.include_role:
    name: tls-certificates
  vars:
    basename: streaming_backend
    common_name: "{{ streaming.backend.server_name }}"
    subject_alt_names: []
    self_sign: false
    letsencrypt_do_nginx_vhost: false
  when: not skip_unit_test
  tags:
    - streaming

- name: Install common Nginx TLS config
  ansible.builtin.include_role:
    name: tls-certificates
    tasks_from: nginx_common
  when: not skip_unit_test
  tags:
    - streaming

- name: Install nginx streaming backend https configuration
  ansible.builtin.template:
    src: nginx/sites-enabled/streaming-backend-https.conf.j2
    dest: /etc/nginx/sites-enabled/streaming-backend-https.conf
    owner: root
    group: root
    mode: "0644"
  notify: Reload nginx
  tags:
    - streaming

- name: Install nginx streaming backend rtmp configuration
  ansible.builtin.template:
    src: nginx/rtmp/backend.conf.j2
    dest: /etc/nginx/rtmp/backend.conf
  notify: Restart nginx
  tags:
    - streaming
    - streaming-backend_config

- name: Create index.html for streaming backend
  ansible.builtin.template:
    src: index.html.j2
    dest: "{{ streaming.backend.data_root }}/www/index.html"
    owner: www-data
    group: www-data
    mode: "0644"
  tags:
    - streaming
