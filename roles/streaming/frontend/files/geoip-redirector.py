#!/usr/bin/python3
# Ansible Managed

import argparse
import logging
from ipaddress import ip_address, ip_network

from aiohttp import hdrs, web
import geoip2.database
import geoip2.errors
import yaml


log = logging.getLogger('geoip-redirector')
routes = web.RouteTableDef()


class Rule:
    def __init__(self, **kwargs):
        self.target = kwargs.pop('target')
        self.prepare(**kwargs)

    def prepare(self):
        pass

    def match(self, address, geo_info):
        return True

    def __str__(self):
        return 'Rule(* -> {})'.format(self.target)


class IPRule(Rule):
    def prepare(self, address):
        self.address = ip_address(address)

    def match(self, address, geo_info):
        return address == self.address

    def __str__(self):
        return 'IPRule({} -> {}>'.format(self.address, self.target)


class IPNetRule(Rule):
    def prepare(self, network):
        self.network = ip_network(network)

    def match(self, address, geo_info):
        return address in self.network

    def __str__(self):
        return 'IPNetRule<{} -> {}>'.format(self.network, self.target)


class CountryRule(Rule):
    def prepare(self, country):
        self.country = country

    def match(self, address, geo_info):
        if not geo_info:
            return False
        return geo_info.country.iso_code == self.country

    def __str__(self):
        return 'CountryRule<{} -> {}>'.format(self.country, self.target)


class ContinentRule(Rule):
    def prepare(self, continent):
        self.continent = continent

    def match(self, address, geo_info):
        if not geo_info:
            return False
        return geo_info.continent.code == self.continent

    def __str__(self):
        return 'ContinentRule<{} -> {}>'.format(self.continent, self.target)


def load_rules(src_rules):
    """Parse the rule set from the YAML config

    Transform a list of dict rule descriptions into a list of Rule() objects.
    """
    output = []
    for src_rule in src_rules:
        target = src_rule['target']
        if 'ip' in src_rule:
            addresses = src_rule['ip']
            if isinstance(addresses, str):
                addresses = [addresses]
            for ip in addresses:
                if '/' in ip:
                    output.append(IPNetRule(network=ip, target=target))
                else:
                    output.append(IPRule(address=ip, target=target))
        elif 'country' in src_rule:
            countries = src_rule['country']
            if isinstance(countries, str):
                countries = [countries]
            for country in countries:
                output.append(CountryRule(country=country, target=target))
        elif 'continent' in src_rule:
            continents = src_rule['continent']
            if isinstance(continents, str):
                continents = [continents]
            for continent in continents:
                output.append(ContinentRule(continent=continent, target=target))
        else:
            output.append(Rule(target=target))
    return output


def lookup(request):
    """Select a target for request, based on the rules"""
    address = request.remote
    reader = request.app['reader']
    rules = request.app['rules']
    try:
        geo_info = reader.city(address)
    except geoip2.errors.AddressNotFoundError:
        log.info('Address Not Found: %s', address)
        geo_info = None
    address = ip_address(address)
    for rule in rules:
        if rule.match(address, geo_info):
            log.debug('Request from: %s: %s', address, str(rule))
            return rule.target


# Replace with aiohttp-cors when it's in Debian
class CORSView(web.View):
    async def options(self):
        return web.Response(headers={hdrs.ACCESS_CONTROL_ALLOW_ORIGIN: '*'})


@routes.view('/redir/{path:.*}')
class RedirectView(CORSView):
    async def get(self):
        target = lookup(self.request)
        path = self.request.match_info['path']
        app = self.request.app
        dest = 'https://{}.{}/{}'.format(target, app['domain'], path)
        return web.Response(status=302, headers={hdrs.LOCATION: dest})


@routes.view('/local-server')
class LocalServerView(CORSView):
    async def get(self):
        target = lookup(self.request)
        return web.Response(text=target)


# Replace with aiohttp-remotes when it's in Debian
@web.middleware
async def remote_addr(request, handler):
    forwarded_for = request.headers.getall(hdrs.X_FORWARDED_FOR, [])
    if forwarded_for:
        if len(forwarded_for) > 1:
            raise Exception('Too many X-Forwarded-For headers')
        forwarded_for = [addr.strip() for addr in forwarded_for[0].split(',')]
        request = request.clone(remote=forwarded_for[-1])
    return await handler(request)


@web.middleware
async def cors_allow_wildcard(request, handler):
    response = await handler(request)
    response.headers[hdrs.ACCESS_CONTROL_ALLOW_ORIGIN] = '*'
    return response


async def app_factory(reader, rules, domain):
    app = web.Application(middlewares=[remote_addr, cors_allow_wildcard])
    app.add_routes(routes)
    app['reader'] = reader
    app['rules'] = rules
    app['domain'] = domain
    return app


def main():
    p = argparse.ArgumentParser('GeoIP Redirector')
    p.add_argument('-c', '--config', type=open,
                   default='/etc/geoip-redirector.conf',
                   help='Configuration file')
    p.add_argument('-v', '--verbose', action='store_true',
                   help='Increase verbosity')
    args = p.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO)
    config = yaml.safe_load(args.config)
    rules = load_rules(config['rules'])
    reader = geoip2.database.Reader(config['geoip_city_db'])

    app = app_factory(reader, rules, config['domain'])
    web.run_app(app)


if __name__ == '__main__':
    main()
