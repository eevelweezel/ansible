# streaming/frontend

Install and configure nginx to act as a streaming frontend server.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

Main variables are:

* `debian_version`:                  Codename of the version of Debian used.

* `streaming.frontend.data_root`:    nginx data_root.

* `streaming.frontend.onsite`:       The FQDN of the onsite streaming frontend.

* `streaming.geoip.domain`:          Domain name containing frontends.

* `streaming.geoip.rules`:           List of GeoIP routing rules.

* `streaming_frontend_aliases`:      Optional list of aliases for the streaming frontend.

* `streaming_frontend_hostname`:     Override the FQDN for a specific
                                     frontend (e.g. if it shares the
                                     backend role).
* `maxmind_license_key`:             License key for MaxMind.com.
                                     Used to download GeoLite2-City.

Other variables used are:

* `letsencrypt_well_known_dir`: Directory where to store the `/.well-known/` data
                                for the Let's Encrypt challenge.

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.

### GeoIP Rules:

Each rule is a dictionary of an optional match rule (`ip`, `country`,
`continent`), and a target. Rules are executed in-order. For sanity, the
last rule should be a default target.

e.g.

```
- ip: 192.0.2.0/24
  target: local
- ip: 2001:db8::/32
  target: local
- country: ZA
  target: ams1
- continent: [NA, SA, AN]
  target: sfo1
- continent: [AS, OC]
  target: sgp1
- continent: [EU, AF]
  target: ams1
- target: ams1
```
