#!/usr/bin/python3
# Manged by ansible.
"""Bulk export etherpads from a Pentbarf XML file."""

import os
import re
import time
import urllib.request

import xml.etree.ElementTree as ET

from argparse import ArgumentParser


p = ArgumentParser(description=__doc__)
p.add_argument('-u', '--url', type=str, help='FQDN of the etherpad instance.')
p.add_argument('-p', '--penta', help='Pentabarf XML file.')
p.add_argument('-f', '--format', choices=['txt', 'html', 'etherpad'],
               help='Format of the exported files.')
p.add_argument('-o', '--output', default=os.getcwd(),
               help='Output directory to save files to.')
p.add_argument('-t', '--time', type=int, default="6",
               help=("Time in seconds between file downloads. Might need to be "
                     "tweaked depending on the server's settings."))
args = p.parse_args()


def get_ids():
    """Extract talk IDs from the Pentabarf XML."""
    talk_ids = []
    with open(args.penta) as xml:
        schedule = ET.XML(xml.read())
        for day in schedule:
            for room in day:
                for row in room:
                    talk = re.sub('^http.*/talks/', '', row.find('url').text)[:-1]
                    if re.match('^[0-9]', talk):
                        split_talk = re.split('-', talk, maxsplit=1)
                        tid = split_talk[0]
                        slug = split_talk[1]
                        if len(slug) > 33:  # Slugs are truncated at 32 chars
                            talk = tid + '-' + slug[:45]
                        talk_ids.append(talk)
    return talk_ids


def download(talk_id):
    """Download an etherpad file."""

    final_url = 'https://' + args.url + '/p/' + talk_id + '/export/' + args.format
    file_name = os.path.abspath(args.output) + '/' + talk_id + '.' + args.format
    attempts = 3
    for attempt in range(attempts):
        try:
            urllib.request.urlretrieve(final_url, file_name)
        except urllib.error.HTTPError as e:
            if e.code == 404:
                print(final_url + ': HTTP Error 404: Not Found')
                return
            elif e.code < 500:
                print(f'{final_url}: HTTP Error {e.code}: {e.reason}')
                raise
            elif attempt == attempts - 1:
                print(f'{final_url}: HTTP Error {e.code}: {e.reason} '
                      f'(retried {attempts} times)')
                raise


def main():
    """Main function."""
    talk_ids = get_ids()
    if not os.path.exists(args.output):
        os.makedirs(args.output)
    for talk_id in talk_ids:
        download(talk_id)
        time.sleep(args.time)


if __name__ == "__main__":
    main()
