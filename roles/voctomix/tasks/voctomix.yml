---
- name: Install voctomix & related packages
  ansible.builtin.apt:
    name:
      - voctomix
      - voctomix-outcasts
      - gstreamer1.0-pulseaudio
      - gstreamer1.0-gl
      - systemd

- name: Create voctomix config dir
  ansible.builtin.file:
    path: /etc/voctomix
    state: directory

- name: Configure voctomix
  ansible.builtin.template:
    src: templates/{{ item }}.j2
    dest: /etc/voctomix/{{ item }}
  with_items:
    - voctocore.ini
    - voctogui.ini

- name: Create recording directory
  ansible.builtin.file:
    state: directory
    dest: /srv/video/{{ org }}/{{ show }}/dv/{{ room_name }}
    recurse: true
    owner: "{{ user_name }}"
    group: "{{ user_name }}"

- name: Push the monitoring script
  ansible.builtin.copy:
    src: files/monitor-stdout
    dest: /usr/local/bin/monitor-stdout
    mode: "0755"

- name: Push the h.264 recording script
  ansible.builtin.copy:
    src: files/videoteam-record-timestamp-mp4
    dest: /usr/local/bin/videoteam-record-timestamp-mp4
    mode: "0755"

- name: Push the midnight mkdir script
  ansible.builtin.copy:
    src: files/videoteam-create-tomorrow
    dest: /usr/local/bin/videoteam-create-tomorrow
    mode: "0755"

- name: Install voctocore systemd unit
  ansible.builtin.copy:
    src: files/systemd/videoteam-voctocore.service
    dest: /etc/systemd/user/videoteam-voctocore.service
  notify: Reload-systemd

- name: Create loop directory
  ansible.builtin.file:
    path: /var/cache/voctomix
    state: directory
    mode: "0775"

- name: Download the sponsor loop
  ansible.builtin.get_url:
    url: "{{ voctomix.loop_url }}"
    dest: /var/cache/voctomix/loop.ts
  when: voctomix.loop_url is defined
  tags:
    - loop

- name: Remove the sponsor loop
  ansible.builtin.file:
    path: /var/cache/voctomix/loop.ts
    state: absent
  when: voctomix.loop_url is undefined
  tags:
    - loop

- name: Download the background loop
  ansible.builtin.get_url:
    url: "{{ voctomix.bgloop_url }}"
    dest: /var/cache/voctomix/bgloop.ts
  when: voctomix.bgloop_url is defined
  tags:
    - loop

- name: Remove the background loop
  ansible.builtin.file:
    path: /var/cache/voctomix/bgloop.ts
    state: absent
  when: voctomix.bgloop_url is undefined
  tags:
    - loop

- name: Install voctomix recording systemd units
  ansible.builtin.template:
    src: templates/videoteam-{{ item }}.j2
    dest: /etc/systemd/user/videoteam-{{ item }}
  with_items:
    - create-tomorrow.service
    - create-tomorrow.timer
    - cutlist.service
    - record.service
    - loop.service
    - bgloop.service
  notify: Reload-systemd

- name: Enable voctomix systemd units
  ansible.builtin.file:
    state: link
    src: /etc/systemd/user/videoteam-{{ item }}
    dest: "/home/{{ user_name }}/.config/systemd/user/xlogin.target.wants\
           /videoteam-{{ item }}"
    owner: "{{ user_name }}"
    group: "{{ user_name }}"
  with_items:
    - create-tomorrow.timer
    - cutlist.service
    - record.service
    - voctocore.service
    - loop.service
    - bgloop.service
  when: not skip_unit_test

- name: Autostart voctogui
  ansible.builtin.file:
    src: /usr/share/applications/voctogui.desktop
    dest: /etc/xdg/autostart/voctogui.desktop
    state: link
  when: voctomix.autostart_gui

- name: Autostart record watch
  ansible.builtin.template:
    src: templates/recordwatch.desktop.j2
    dest: /etc/xdg/autostart/recordwatch.desktop
    mode: "0755"
