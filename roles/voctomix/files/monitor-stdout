#!/usr/bin/python3
# Ansible Managed

import select
import signal
import subprocess
import sys


def flush_fds(fds, size=None):
    """Flush the fds in the given list to the arguments"""
    for fdin, fdout in fds.values():
        fdout.write(fdin.read(size))
        fdout.flush()


def proxy_signals(process):
    """Proxy any signals to the child process, ignoring them ourselves"""
    # strsignal only added in 3.8:
    signals = {
        signal.SIGINT: 'SIGINT',
        signal.SIGTERM: 'SIGTERM',
        signal.SIGPIPE: 'SIGPIPE',
        signal.SIGQUIT: 'SIGQUIT',
    }

    def signal_handler(signum, frame):
        if process.poll() is None:
            signame = signals[signum]
            print(f'Caught a {signame}, passed to child', file=sys.stderr)
            process.send_signal(signum)

    for sig in signals:
        signal.signal(sig, signal_handler)


def main():
    timeout = float(sys.argv[1])
    cmdline = sys.argv[2:]

    with subprocess.Popen(cmdline, stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE, bufsize=0) as process:
        proxy_signals(process)
        fd_map = {
            process.stdout.fileno(): (process.stdout, sys.stdout.buffer),
            process.stderr.fileno(): (process.stderr, sys.stderr.buffer),
        }
        while True:
            retcode = process.poll()
            if retcode is not None:
                flush_fds(fd_map)
                sys.exit(retcode)
            r_fds, _, _ = select.select(list(fd_map.keys()), [], [], timeout)
            if not r_fds:
                print("Process didn't send any output for %s seconds, "
                      "terminating" % timeout, file=sys.stderr)
                process.kill()
                sys.exit(128)
            else:
                flush_fds({fileno: fd_map[fileno] for fileno in r_fds}, 1024)


if __name__ == '__main__':
    main()
