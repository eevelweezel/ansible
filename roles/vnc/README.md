# vnc

This role installs and configures a VNC server to enable remote desktop
logging on a machine.

## Tasks

The tasks are divided this way:

* `tasks/tls.yml`: Manage the generation of the TLS certificate.

* `tasks/vnc.yml`: Install and configure the VNC server.

Everything is in the `tasks/main.yml` file.

## Available variables

List of the role's variables. Please follow the following style:

Main variables are:

* `user_name`: String. Main user username.

* `vnc_dummy`: Boolean. Use when the target machine doesn't have any other
               graphical drivers (intel, Nvidia, etc.) to configure X
               accordingly.

* `vnc_password`: String. Password to connect to the VNC server.

* `vnc_port`: Integer. TCP port to connect to the VNC server.

* `vnc_tls`: Boolean. Enable TLS on the VNC server.

Other variables used are:

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
