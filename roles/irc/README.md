# irc

Manage IRC.

## Tasks

The tasks are divided this way:

* `tasks/main.yml`: Manages IRC connection via Konversation.


## Available variables

Main variables are:

* `irc.network`:         Name of the IRC server to connect to.

* `irc.server`:          Domain name of the IRC server to connect to.

* `irc.ssl_port`:        Integer. SSL port of the IRC server to connect to.

* `irc.global_channels`: List. Channels to connect to.

* `irc_nick`:            IRC nickname.

* `irc_autostart`:       Start the IRC client on login.

Other variables used are:

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
