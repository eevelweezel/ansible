#!/bin/sh
# {{ ansible_managed }}

set -euf

# this script is both a template and a script.

# the repositories are checked out in late_command.sh, which runs from d-i
# the checkout here is only for convenience if you manually use this script on
# a machine that was installed some other way
# if the repositories already exist, these variables are not used

playbook_repo="{{ playbook_repo }}"
playbook_branch="{{ playbook_branch }}"
inventory_repo="{{ inventory_repo }}"
inventory_branch="{{ inventory_branch }}"

# https://salsa.debian.org/debconf-video-team/ansible/-/issues/117
# ansible-up inventory_repo_dir not used
inventory_repo_dir="{{ inventory_repo_dir }}"

BASE=/root
PB=$BASE/playbook-repo
PLAYBOOKS=$PB/site.yml
INV=$BASE/inventory-repo
INVENTORY=$INV/inventory/hosts
VAULTPW=$BASE/.ansible-vault

# clone or update our ansible repository(s)

cd $BASE
if [ -d $PB ]
then
    cd $PB
    git pull --ff-only
else
    git clone $playbook_repo $PB
    cd $PB
    git checkout $playbook_branch
fi
TAG_INFO="PLAYBOOK: `git describe --dirty --always`"

cd $BASE
if [ -d $INV ]
then
    cd $INV
    git pull --ff-only
else
    git clone $inventory_repo $INV
    cd $INV
    git checkout $inventory_branch
fi

TAG_INFO="$TAG_INFO INVENTORY: `git describe --dirty --always`"

cd $BASE
echo "`date` $TAG_INFO" >> ansible.ran

vault_arg=
if [ -f $VAULTPW ]; then
    vault_arg="--vault-password-file=$VAULTPW"
else
    vault_arg="--skip-tags=vault"
fi

ansible-playbook \
    --diff --inventory-file=$INVENTORY \
    ${vault_arg} \
    --connection=local \
    --limit=$(hostname) \
    $PLAYBOOKS \
    "$@"

