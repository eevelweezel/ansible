# obs

Manage and configure OBS.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

Main variables are:

* `obs_public_ip`: Public IP of the obs machine.

* `voctomixes`: List of dicts. Voctomix machines to send the loop to.

* `voctomixes.[].host`: FQDN of a voctomix machine.

* `voctomixes.[].port`: TCP port of the voctomix machine.

* `voctomixes.[].obs_push_to_voctomix`:  Boolean. Have OBS push to voctomix
                                         machine.

Other variables used are:

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
